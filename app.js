var video = document.getElementById('video');
var canvas = document.getElementById('overlay');
var context = canvas.getContext('2d');
var button = document.getElementById('button');
var gallary = document.getElementById('gallary');
var isTracking = false;
var imageData;
var scaleFactor=1.00;
var mosaicSize;
var constraints = {
  audio: false,
  video: {
    // スマホのバックカメラを使用
    facingMode: 'environment'
  }
};
var track = new clm.tracker({
  useWebGL: true
});
var roseImage = new Image;

function successFunc (stream) {
  if ('srcObject' in video) {
    video.srcObject = stream;
  } else {
    window.URL = window.URL || window.webkitURL;
    video.src = (window.URL && window.URL.createObjectURL(stream));
  }
  // 動画のメタ情報のロードが完了したら実行
  video.onloadedmetadata = function() {
    adjustProportions();
    startTracking();
  };
  video.onresize = function() {
    adjustProportions();
    if (isTracking) {
      track.stop();
      track.reset();
      startTracking();
    }
  };
};

function startTracking() {
  // トラッキング開始
  track.start(video);
  drawLoop();
  isTracking = true;
}

function adjustProportions() {
  var ratio = video.videoWidth / video.videoHeight;

  if (ratio < 1) {
    // 画面縦長フラグ
    isPortrait = false;
  }
  video.width = Math.round(video.height * ratio);
  canvas.width = video.width;
  canvas.height = video.height;
}

function displaySnapshot() {
  var snapshot = new Image();

  snapshot.src = canvas.toDataURL('image/png');
    snapshot.onload = function(){
      snapshot.width = snapshot.width / 2;
      snapshot.height = snapshot.height / 2;
      gallary.appendChild(snapshot);
    }
}

function drawLoop() {
  // 描画をクリア
  context.clearRect(0, 0, canvas.width, canvas.height);
  // videoをcanvasにトレース
  context.drawImage(video, 0, 0, canvas.width, canvas.height);
  // canvasの情報を取得
  imageData = context.getImageData(0, 0, canvas.width, canvas.height);

  requestAnimationFrame(drawLoop);
}

function createMosaic(mosaicSize) {
  for (y = 0; y < canvas.height; y = y + mosaicSize) {
    for (x = 0; x < canvas.width; x = x + mosaicSize) {
      // getImageData で取得したピクセル情報から該当するピクセルのカラー情報を取得
      var cR = imageData.data[(y * canvas.width + x) * 4];
      var cG = imageData.data[(y * canvas.width + x) * 4 + 1];
      var cB = imageData.data[(y * canvas.width + x) * 4 + 2];

      context.fillStyle = 'rgb(' +cR+ ',' +cG+ ',' +cB+ ')';
      context.fillRect(x, y, x + mosaicSize, y + mosaicSize);
    }
  }
}

pModel.shapeModel.nonRegularizedVectors.push(9);
pModel.shapeModel.nonRegularizedVectors.push(11);

track.init(pModel);

// カメラから映像を取得
if (navigator.mediaDevices) {
  navigator.mediaDevices.getUserMedia(constraints)
    .then(successFunc)
    .catch((err) => {
      window.alert(err.name + ': ' + err.message);
  });
} else {
  window.alert('非対応ブラウザです');
}

function zoomIn() {
  scaleFactor*=1.1;
  drawLoop();
}

function zoomOut() {
    if(scaleFactor > 1.00){
    scaleFactor/=1.1;
    drawLoop();
  }else{
    scaleFactor=1.00;
  }
  drawLoop();
}

// 保存ボタンを押したら実行
button.addEventListener('click', displaySnapshot);